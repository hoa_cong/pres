class CreateTextBlocks < ActiveRecord::Migration
  def change
    create_table :text_blocks do |t|
      t.string :block_type
      t.integer :block_id
      t.belongs_to :slide, index: true
      t.timestamps null: false
    end
  end
end
