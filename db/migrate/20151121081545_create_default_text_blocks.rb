class CreateDefaultTextBlocks < ActiveRecord::Migration
  def change
    create_table :default_text_blocks do |t|
      t.integer :slide_id
      t.float :width
      t.float :height
      t.float :position_x
      t.float :position_y
      t.string :background
      t.string :content
      t.belongs_to :slide, index:true
      t.belongs_to :textblock

      t.timestamps null: false
    end
  end
end
