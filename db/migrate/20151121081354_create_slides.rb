class CreateSlides < ActiveRecord::Migration
  def change
    create_table :slides do |t|
      t.integer :order
      t.float :width
      t.float :height
      t.string :background

      t.timestamps null: false
    end
  end
end
