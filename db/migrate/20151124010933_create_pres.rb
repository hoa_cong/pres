class CreatePres < ActiveRecord::Migration
  def change
    create_table :pres do |t|
      t.string :title
      t.string :description
      t.integer :project_id

      t.timestamps null: false
    end
  end
end
