class UsersController < ApplicationController
  def show
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = 'Welcome to my page'
      redirect_to :root
    else
      @user.errors.to_hash.each do |field, error|
        instance_variable_set('@' + field.to_s + '_error', error)
      end
      render 'new'
    end
  end

  def update
  end

  def destroy
  end


  private

    def user_params
      params.require(:user).permit(:email,:password, :password_confirmation)
    end
end
