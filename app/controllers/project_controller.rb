class ProjectController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def create
    Project.create(project_params)
    redirect_to :root
  end

  def index
  end

  def presentations_show
    project = Project.find_by_id(params[:project_id])
    @presentations = project.pres
    pres_rendering = Hash.new
    @presentations.each_with_index do |pre, index|
      pres_rendering[index] = {pre_id: pre.id, title: pre.title, description: pre.description, }
    end
    render json: pres_rendering
  end

  private
    def project_params
      params.require('project').permit(:title, :description)
    end
end
