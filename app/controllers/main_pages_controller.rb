class MainPagesController < ApplicationController
  def mainpage
    @projects = Project.all
    @project_presentations = Hash.new
    @projects.each do |project|
      @project_presentations["project-#{project.id}"] = project.pres.to_a.each do |pre|
        {id: pre.id, title: pre.title, description: pre.description}
      end
    end
  end
end
