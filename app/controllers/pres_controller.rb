class PresController < ApplicationController
  def show
    @presentation = Pre.find_by_id(params[:id])
  end
end
