$(document).ready(handling_slide_page);
$(document).on('page:load', handling_slide_page);


function handling_slide_page(){
  hide_add_block_button();
}


function hide_add_block_button(){
  $('#add-block-button').mouseover(function(){
    $(this).removeClass('disabled');
  }).on('click', function(){
    add_block();
  }).mouseout(function(){
    $(this).addClass('disabled');
  });
}


function add_block(){
  var new_block_html = "<div class='new-el'><textarea style='background:white;'></textarea></div>"
  $('#slide-frame').append(new_block_html);
  $('.new-el').draggabilly({
  // options...
  });
}
