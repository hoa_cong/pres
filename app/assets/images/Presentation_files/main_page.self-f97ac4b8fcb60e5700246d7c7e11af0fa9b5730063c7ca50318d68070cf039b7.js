$(document).ready(main_page_fix);
$(document).on('page:load', main_page_fix);


function main_page_fix(){
  var $container = $("#container").packery({
    columnWidth: 80,
    rowHeight: 200
  });

$container.find('.item').each( function( i, itemElem ) {
  // make element draggable with Draggabilly
  var draggie = new Draggabilly( itemElem );
  // bind Draggabilly events to Packery
    $container.packery( 'bindDraggabillyEvents', draggie );
  });

}


function search_project_func(){
  $('#new-project').fadeOut('fast',function(){
    $('#search-project').fadeToggle('slow', function(){
    });
  });
}

function create_probject_func(){
  $('#search-project').fadeOut('fast', function(){
    $('#new-project').fadeToggle('slow', function(){
    });
  });
}
