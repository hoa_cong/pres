$(document).ready(handling_slide_page);
$(document).on('page:load', handling_slide_page);


///// text block html
function default_text_block_html(){
  var html = "<div class='text-block'>"
  html +=       "<div class='handler'>"
  html +=           "<div class='text-block-draggable'>drag</div>"
  html +=           "<div class='text-block-editable'>edit</div>"
  html +=           "<i class='text-block-removable fa fa-close' title='Edit'></i>"
  html +=       "</div>"
  html +=       "<div class='text-block-body'>"
  html +=         "<pre><span></span><br></pre>"
  html +=         "<textarea class='text-area'></textarea>"
  html +=       "</div>"
  html +=   "</div>";
  return html;
}


//// text block
function editable_text_block(){
  var html = "<div class='editable-textblock'>"
  html +=       "<div class='handler'>"
  html +=           "<div class='text-block-draggable'>drag</div>"
  html +=           "<div class='text-block-editable'>Edit</div>"
  html +=           "<i class='text-block-removable fa fa-close' title='Edit'></i>"
  html +=       "</div>"
  html +=       "<div class='editable-textblock-body'></div>"
  html +=    "</div>"
  return html;
}

/// code block

function codeblock_html(id){
  var html = "<div data-value='edit-mode' id='code-block-" + id + "' class='code-block'>"
  html +=       "<div class='handler'>"
  html +=         "<div class='code-block-draggable'>drag</div>"
  html +=         "<div class='code-block-language'>Apply</div>"
  html +=         "<i class='code-block-removable fa fa-close' title='Edit'></i>"
  html +=       "</div>"
  html +=       "<div class=code-block-body>"
  html +=       "</div>"
  html +=    "</div>"
  return html;
}


function resize_text_block_html(){
  var html = "<div class='text-block'><div class='handle'></div><input class='text-area' style='font-size: 5vw;display: flex;align-items: center;justify-content: center;text-align: center;line-height: 1;padding: 0;margin: 0;'></input><div class='block-menu'></div></div>";
  return html;
}
/////////////

//// images block html

function link_to_images(){ // for testing
  var link1 = 'http://dougleschan.com/the-recruitment-guru/wp-content/uploads/2015/08/Beautiful-Wallpapers-14.jpg';
  var link2 = 'http://www.whenwasitinvented.org/wp-content/uploads/2011/08/Blackboard.jpg';
  var link3 = 'https://fbjunksblog.files.wordpress.com/2012/12/2-beautiful_flower_heart_love-11.jpg';
  var link4 = 'https://fbjunksblog.files.wordpress.com/2012/12/2-beautiful_flower_heart_love-11.jpg';
  var link5 = 'http://dougleschan.com/the-recruitment-guru/wp-content/uploads/2015/08/Beautiful-Wallpapers-14.jpg';
  return [link1, link2, link3, link4, link5];

}

function color_names(){
  var colors = ["#F0F8FF","#FAEBD7","#00FFFF","#7FFFD4", "#F0FFFF","#F5F5DC","#FFE4C4","#000000", "#A52A2A","#5F9EA0", "#7FFF00","#FF7F50", "#6495ED", "#FFF8DC", "#DC143C", "#008B8B", "#A9A9A9","#E9967A","#8FBC8F", "#483D8B","#2F4F4F"];
  return colors;
}
function generate_image_box(image_links, color_names){
  var html = [];
  $.each(image_links, function(index, image_link){
    html.push("<div class='col-xs-12 image-block'><img class='images' src='" + image_link + "' height='100' width='100%' /></div>");
  });
  html = html.join('');
  var background_box = "<div class='col-xs-8' style='height:400px; overflow: auto;'>" + html +"</div>";
  html = []
  $.each(color_names, function(index, color){
    html.push("<div class='col-xs-12' style='padding: 0px'><div class='color-block' style='background:"+ color +"'></div></div>");
  });
  var color_box = "<div class='col-xs-4' style='height:400px; overflow: auto;'>" + html.join('') +"</div>"
  return background_box  + color_box;
}

/////////

function handling_slide_page(){
  slide_frame_handling();
  utility_buttons_handler();
  menu_options_showing();
  block_decorator_init();
}

function generate_rgba(num){
  var rand_color, rand_opacity, rgba;
  var opacity = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1];
  var rgba_list = [];
  for (i = 0; i < num; i++){
    rgba = "rgba(";
    for (j = 0; j < 3; j++){
      rand_color = Math.floor((Math.random() * 256));
      rgba += (rand_color + ",");
    }
    rand_opacity = opacity[(Math.floor((Math.random() * 10) + 1))]
    rgba += (rand_opacity + ")")
    rgba_list.push(rgba);
  }
  return rgba_list;
}

function block_decorator_init(){
  var num_of_color = 100;
  var $block_decorator_option = $('#block-decorator-options');
  var list_of_rgba = generate_rgba(num_of_color);
  var list_of_new_els = $.map(list_of_rgba, function(el){
    return "<div style='background-color:" + el + "'></div>"
  });

  var $border_color = $block_decorator_option.find('.border-color .color-options');
  $.each(list_of_new_els, function(index, el){
    $border_color.append(el).css('width', 40*num_of_color);
  })
  var $background_color = $block_decorator_option.find('.background-color .color-options');
  $.each(list_of_new_els, function(index, el){
    $background_color.append(el).css('width', 40*num_of_color);
  })

}


function slide_frame_handling(){
  $('#slide-frame').resizable({
    containment: "parent",
    minWidth: 600,
    minHeight: 400
  });

  $('#slide-frame-wrapper').draggabilly({
    handle: '#slide-draggable',
    containment: '#slide-container-body'
  })
}

function menu_options_showing(){
  $('.menu-options').bind("mouseenter click" ,function(){
    $(this).parent().mouseleave(function(){
      if (! $('.menu-options').next().next().is(":visible")){
        $('.menu-options').next().hide(100);
      }
    });
    $(this).next().show('slide', {direction:'left'}, 300, function(){
      $(this).unbind('click').bind('click',function(){
        if ($(this).attr('id') == 'changing-background-image'){
          changing_background_image();
        }
      });
    }).on('blur',function(){
      $(this).hide(200);
      $('#image-folder').empty().hide();
    });
  });
}


function changing_background_image(this_obj){
  $('#image-folder').show(100).append(generate_image_box(link_to_images(), color_names()));
  var background;
  var type;
  if ($('#slide-frame').css('background-image') !== 'none'){
    background = $('#slide-frame').css('background-image');
    type = 'image';
  }else{
    background = $('#slide-frame').css('background-color');
    type = 'color';
  }
  $('.images, .color-block').hover(function(){
    var hover_background;
    if($(this).attr('class') === 'images'){
      hover_background = "url(" +  $(this).prop("src")+ ")";
      change_to_image_background(hover_background, background, type, $('#slide-frame'));
    }
    else{
      hover_background = $(this).css('background-color');
      change_to_color_background(hover_background, background, type, $('#slide-frame'));
    }
  }).click(function(){
    if($(this).attr('class') === 'images'){
      background = "url(" +  $(this).prop("src")+ ")";
    }
    else{
      background = $(this).css('background-color');
    }
    $('#changing-background-image').hide(10, function(){
      $(this).next().empty().hide();
    })

  }).mouseout(function(){
    if (type === 'image'){
      change_to_image_background(background, background, type, $('#slide-frame'));
    }
    else if (type === 'color'){
      change_to_color_background(background, background, type, $('#slide-frame'))
    }
  });

}

function change_to_image_background(image, current_background,type, object){
  object.css('background-image', image);
}

function change_to_color_background(color, current_background,type, object){
  object.css('background', color);
  if (type === 'image'){
    object.css('background-image', 'none');
  }
}


var counter = 0;
//////////// utility buttons handler ///////////////////
function utility_buttons_handler(){
  $('#functionality-buttons a span').hover(function(){
    $(this).removeClass('fa-2x').addClass('fa-3x');
  }).mouseleave(function(){
    $(this).removeClass('fa-3x').addClass('fa-2x');
  }).unbind('click').bind('click', function(){
    var id = $(this).parent().attr('id');
    if (id === 'add-default-textblock-button'){
      add_text_block();
    }
    else if (id === 'add-editable-textblock-button'){
      add_editable_textblock(counter++);
    }
    else if (id === 'add-codeblock-button'){
      add_codeblock();
    }
    else if (id ==='add-image-block'){
      add_image_block()
    }
  });
}

////////image block

function image_block_html(){
  var html = "<div class='image-slide-block'>"
  html +=       "<ul class='handler'>"
  html +=         "<li class='image-block-draggable'>drag</li>"
  html +=         "<li class='image-url-uploadable'>"
  html +=            "<a class='url-upload-button'>url</a>"
  html +=            "<ul class='image-url-upload'>"
  html +=               "<li class='image-url-uploadable-button'><button>ok</button></li>"
  html +=               "<li class='url-placeholder'><input placeholder='image url'></input></li>"
  html +=            "</ul>"
  html +=         "</li>"
  html +=         "<li class='image-file-uploadable'><a>File</a>"
  html +=             "<form class=image-file-upload>"
  html +=               "<input class='image-file-uploadable-button' type='file' name='pic' accept='image/*'></input>"
  html +=             "</form>"
  html +=         "</li>"
  html +=         "<li class='image-block-removable'><i class='fa fa-close' title='Edit'></i></li>"
  html +=       "</ul>"
  html +=       "<div class='image-block-body'>"
  html +=         "<img src='http://images6.fanpop.com/image/photos/38500000/beautiful-wallpaper-1-beautiful-pictures-38538866-2560-1600.jpg' alt='' >"
  html +=       "</div>"
  html +=     "</div>"
  return html;
}

function image_block_decoration_html(){
  var html = "<div class='image-block-decorator'>"
  html +=       "<li class='image-decorator-border'>"
  html +=           "<span>Border width</span>"
  html +=           "<span class='image-border-size-adjustable'>"
  html +=               "<i class='fa fa-arrow-up' title='Edit'>"
  html +=               "<i class='fa fa-arrow-down' title='Edit'>"
  html +=           "</span>"
  html +=           "<span>Border Radius</span>"
  html +=           "<span class='image-border-radius-adjustable'>"
  html +=               "<i class='fa fa-arrow-up' title='Edit'>"
  html +=               "<i class='fa fa-arrow-down' title='Edit'>"
  html +=           "</span>"
}

function add_image_block(){
  $("#slide-frame").append(image_block_html());
  $new_image_block = $("#slide-frame").find('.image-slide-block').last();
  var $handler = $new_image_block.find('.handler');
  var $url_uploadable = $new_image_block.find('.image-url-uploadable');
  var $file_uploadable = $new_image_block.find('.image-file-uploadable');
  var $image_removable = $new_image_block.find('.image-block-removable');
  var $image_body = $new_image_block.find('.image-block-body img');
  var $block_decorator = $('#block-decorator');

  $new_image_block.draggable({
    containment: '#slide-frame',
    cursor: "crosshair",
    handle: ".image-block-draggable",
    start: function(){
      $url_uploadable.find('.image-url-upload').hide();
      $file_uploadable.find('.image-file-upload').hide();
    }
  }).resizable({
    containment: '#slide-frame',
    minWidth: 150,
    minHeight: 100,
    start: function(){
      $(this).find('.handler').hide();
      $url_uploadable.find('.image-url-upload').hide();
      $file_uploadable.find('.image-file-upload').hide();
    },
    resize: function(){

    },
    stop: function(){
      $(this).find('.handler').show();
      this.style.removeProperty('height');
    }
  }).css('position', 'absolute')
  .resizable('disable')
  .on('dblclick', function(){
    $(this).resizable('enable');

    $handler.show(0, function(){
      $image_removable.children().off().on('click', function(){
        $new_image_block.remove();
        //send ajax request
      })
      $url_uploadable.children().first().off().on('click', function(){
        var $upload_area = $(this).next();
        $file_uploadable.find('.image-file-upload').hide();
        $upload_area.show(0, function(){
          var $upload_button = $(this).find('.image-url-uploadable-button')
          var $url_input = $(this).find('.url-placeholder input')
          $upload_button.off().on('click', function(){
            var new_image_url = $url_input.val();
            $image_body.attr('src', new_image_url)
            $upload_area.hide();
          })
        });
      })

      $file_uploadable.children().first().off().on('click',function(){
        var $upload_area  = $(this).next();
        $url_uploadable.find('.image-url-upload').hide();
        $upload_area.show(0,function(){
          var $image_upload = $(this).find('.image-file-uploadable-button');
          $image_upload.change(function(){
            readURL(this);
            $upload_area.hide();
          });
        })
      });

      $image_body.off().on('click', function(){
        $url_uploadable.find('.image-url-upload').hide();
        $file_uploadable.find('.image-file-upload').hide();
      })

      $(document).off().on('mousedown', function(e){
        var $all_descendants = $new_image_block.find('*');
        var $block_decorator_descendants = $block_decorator.find('*');
        if (!all_descendant_clicked($all_descendants, e.target) && !all_descendant_clicked($block_decorator_descendants, e.target)){
          $url_uploadable.find('.image-url-upload').hide();
          $file_uploadable.find('.image-file-upload').hide();
          $handler.hide();
          $new_image_block.resizable('disable');
          $(this).unbind('mousedown');
          unbind_action($block_decorator, 'click')

        }
        else {
          var $block_border_resizing = $block_decorator.find('.block-decorator-border-size');
          var $block_border_radius = $block_decorator.find('.block-decorator-border-radius');
          var $block_border_color = $block_decorator.find('.border-color .color-options');
          var $block_background_color = $block_decorator.find('.background-color .color-options');
          $block_border_resizing.find('.fa-arrow-up').off().on('click', function(){
            var border_width = parseFloat($image_body.css('border-width'));
            $image_body.css('border-width', border_width + 1);
          });
          $block_border_resizing.find('.fa-arrow-down').off().on('click', function(){
            var border_width = parseFloat($image_body.css('border-width'));
            $image_body.css('border-width', border_width - 1);
          });
          $block_border_radius.find('.fa-arrow-up').off().on('click', function(){
            var border_width = parseFloat($image_body.css('border-radius'));
            $image_body.css('border-radius', border_width + 1);
          });
          $block_border_radius.find('.fa-arrow-down').off().on('click', function(){
            var border_width = parseFloat($image_body.css('border-radius'));
            $image_body.css('border-radius', border_width - 1);
          });
          $block_border_color.find('div').off().on('click', function(){
            console.log(4)
            $image_body.css('border-color', $(this).css('background-color'));
          })

          $block_background_color.find('div').off().on('click', function(){
            console.log(3)
            $image_body.css('background-color', $(this).css('background-color'));
          })

        }
      })
    });
  })

}

function unbind_action($elements, action){
  $.each($elements, function(index, element){
    $(element).unbind(action);
  });
}

function all_descendant_clicked($decendants, target){
  for (i = 0; i < $decendants.length; i ++){
    if ($decendants.eq(i).is(target)){
      return true;
    }
  }
  return false;
}


function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.image-block-body img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}



/////////// text edit buttons
function text_edit_buttons_handler(){
  $('#text-edit-buttons li a span').hover(function(){
    $(this).removeClass('fa-2x').addClass('fa-3x');
  }).mouseleave(function(){
    $(this).removeClass('fa-3x').addClass('fa-2x');
  })
}



////// code-block ////


function add_codeblock(){
  var id  = 0;
  $('#slide-frame').append(codeblock_html(id));
  var $code_block = $('#code-block-' + id);
  var lang = 'python';
  var $code_body = $code_block.find('.code-block-body');
  $code_body.find('ql-editor').addClass(lang);
  var quill = new Quill($code_body.get(0));
  $code_block.find('.code-block-language').off().on('click',function(){
    $code_body.each(function(i, block) {hljs.highlightBlock(block);});
    quill = new Quill($code_body.get(0));
  })


}

var quilll


//// editable textblock

function new_toolbar_for_editable_textblock(id){
  var $new_toolbar = $('.text-edit-buttons').clone();
  $new_toolbar.attr('id', 'textblock-toolbar-' + id);
  $new_toolbar.appendTo($('#text-edit-toolbar'));
  return  $new_toolbar.attr('id')
}
function add_editable_textblock(id){
  $('#slide-frame').append(editable_text_block());
  $('.editable-textblock').css('position', 'absolute');
  var $new_block = $('.editable-textblock').last();
  var $block_body = $new_block.find('.editable-textblock-body');
  var quill = new Quill($block_body.get(0));
  var toolbar_id = "#" + new_toolbar_for_editable_textblock(id)
  quill.addModule('toolbar', { container: toolbar_id});
  quilll = quill;
  var $editor = $new_block.find('.ql-editor');
  custom_format_size_text(quill);
  custom_format_font_text(quill);
  var original_height = 0;
  $new_block.resizable({
    containment: '#slide-frame',
    minWidth: 30,
    start: function(event, ui){
      original_height = ui.size.height;
      $(this).find('.handler').hide();
    },
    resize: function(event, ui){
      if (ui.size.height < original_height - 18) {
        var regex = /<div><br><\/div>$/;
        original_height = ui.size.height
        var html = $editor.html();
        if (regex.test(html)){
          html = html.slice(0, -15)
          $editor.html(html)
        }
      }
    },
    stop: function(event, ui){
      if (ui.size.height > original_height){
        var diff = ui.size.height - original_height;
        var quotient = Math.floor(diff/20);
        var appending_html = "";
        for (i = 0; i < quotient; i++){
          appending_html += "<div><br></div>";
        }
        $(this).find('.ql-editor').append(appending_html);
      }
      $(this).find('.handler').show(0);
      this.style.removeProperty('height');
    }
  }).draggable({
    containment: '#slide-frame',
    cursor: "crosshair",
    handle: ".text-block-draggable"
  }).on('mouseenter', function(){
    text_block_show_top_bar($(this), toolbar_id);
  }).on('focusin', function(){
    $(toolbar_id).show();
  }).on('focusout',function(event){
  })
  $new_block.find('.handler .text-block-editable').off().on('click', function(){
    var content = quill.getText();
    var code_regex = /'''\w+'''/
    if (true){
      console.log(3);
      var codes = find_code(content);
      var code_obj;
      while(codes.length > 0){
        console.log(4)
        code_obj = codes.pop();
        start = code_obj.start;
        end = code_obj.end;
        lang = code_obj.lang;
        for (i = start + 1; i < end; i ++){
          $editor.children().eq(i).addClass(lang);
        }
        $editor.children().eq(end).remove();
        $editor.children().eq(start).remove();
        $editor.find('.' + lang).each(function(i, block) {hljs.highlightBlock(block);});
      }
    }
  })

}

function find_code(content){
  var regex = /.*\n/;
  var regexcode_start = /'''\w+.*\n/;
  var regexcode_end = /'''\n/
  var regex_line_code = /'''\w+'''/;
  var list_of_codes = []
  var line, lang, start, end;
  var counter = 0;
  while (regex.test(content)){

    line = regex.exec(content).toString();
    content = content.slice(line.length);
    if (regexcode_start.test(line)){
      start = counter;
      lang = /\w+/.exec(line).toString();
      while(content.length !== 0){
        counter++;
        line = regex.exec(content)
        if (line === null){
          break;
        }
        else{
          line = line.toString();
        }
        content = content.slice(line.length);
        if (regexcode_end.test(line)){
          end = counter;
          list_of_codes.push({lang: lang, start: start, end: end})
          code = "";
          break;
        }
      }
    }
    counter++;
  }
  return list_of_codes;
}
var quilll
/// format quill text

function custom_format_font_text(quill){

  $('.font-options a p').off('click').on('click',function(){
    quill.focus();
    var range = quill.getSelection();
    var start = range.start;
    var end = range.end;
    if (start === end){
      quill.prepareFormat('font', $(this).attr('data-value'));
    }
    else {
      quill.formatText(start, end, {
        'font': $(this).attr('data-value')
      })
    }
  })
}

function custom_format_size_text(quill){
  $('.size-options a span').off('click').on('click', function(){
    quill.focus();
    var range = quill.getSelection();
    var start = range.start;
    var end = range.end;
    if (start === end){
      quill.prepareFormat('size',$(this).attr('data-value'))
    }
    else {
      quill.formatText(start, end, {
        'size': $(this).attr('data-value')
      })
    }
  });
}
//// text block///////
function add_default_block(){
  $('#slide-frame').append(default_text_block_html());
  $('.text-block').resizable({ //enable resize
    containment: '#slide-frame'
  }).on('resize',function(){ //on  resize, change size of top icons
    var $handler = $(this).find('.handler');
    $handler.find('.text-block-removable').css('font-size', $handler.css('height'));
  })
  .on('mouseenter hover focusin', function(){ // show top toolbar
    $(this).find('.handler').show(0,function(){
      $(this).parent().draggabilly({
        handle: '.text-block-draggable',
        containment: '#slide-frame'
      })
    });
    $(this).find('.handler').find('.text-block-removable').on('click',function(){
      $(this).parent().parent().remove();
    });
  }).on('focusout mouseleave', function(event){ // hide top toolbar
    $(this).find('.handler').hide();

  });


}

/////////


function add_text_block(){
  $('#slide-frame').append(default_text_block_html());
  $('.text-block').resizable({
    containment: '#slide-frame',
    minWidth: 200,
    resize: function(event, ui){
      // top size
      var top_height = $(this).find('.handler').css('height');
      top_height = parseFloat(top_height);

      //text body size
      var text_body = $(this).find('.text-block-body');
      var text_body_height = text_body.css('height');
      text_body_height = parseFloat(text_body_height);

      if (ui.size.height <= text_body_height + top_height){
        ui.size.height = (handle_shrinking_text_block($(this), ui.size.height, top_height + text_body_height) || ui.size.height);
      }
      else {
        handle_expanding_text_block($(this),ui.size.height,top_height + text_body_height);
      }
    },
    stop: function(event, ui){
      this.style.removeProperty('height');
      //save to database
    }
  }).draggable({
    containment: '#slide-frame',
    cursor: "crosshair",
    handle: ".text-block-draggable"
  }).on('mouseenter', function(){
    text_block_show_top_bar($(this), toolbar_id);
    $(this).on('mouseleave', function(){
      $(this).find('.handler').hide();

    })
  }).on('focusin', function(){
    $(this).on('input', function(){

    })
  })

  makeExpandingArea($('.text-block-body').last()[0]);
}

function text_block_show_top_bar($text_block, toolbar_id){ // hide text block top bar
  $text_block.find('.handler').show(0,function(){
    $(this).find('.text-block-removable').on('click',function(){
      $(this).parent().parent().remove();
      $(toolbar_id).remove();
    })
  });
}


function handle_shrinking_text_block($text_block, new_height, current_height){
  var $textarea = $text_block.find('textarea');
  var $span = $text_block.find('pre span');
  var content = $textarea.val();
  var regex = /\n$/;
  if (regex.test(content)){
    content = content.slice(0, -1);
    $span.text(content);
    $textarea.val(content);
    return false
  }
  else{
    return current_height;
  }
}

function handle_expanding_text_block($text_block,new_height, current_height){
  var $textarea = $text_block.find('textarea');
  var $span = $text_block.find('pre span');
  var content = $textarea.val();
  if (new_height - current_height > 13){ // if greater than fontsize: add new line;
    $span.text(content + "\n")
    $textarea.val(content+ "\n")
  }

}

function makeExpandingArea(container) {
 var area = container.querySelector('textarea');
 var span = container.querySelector('span');
 if (area.addEventListener) {
   area.addEventListener('input', function() {
     span.textContent = area.value;
   }, false);
   span.textContent = area.value;
 } else if (area.attachEvent) {
   // IE8 compatibility
   area.attachEvent('onpropertychange', function() {
     span.innerText = area.value;
   });
   span.innerText = area.value;
 }
// Enable extra CSS
container.className += " active";
}
