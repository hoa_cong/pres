class Slide < ActiveRecord::Base
  belongs_to :prep
  has_many :text_blocks
  has_many :default_text_blocks, through: :text_blocks
end
