class TextBlock < ActiveRecord::Base
  belongs_to :slide
  has_one :default_text_blocks
end
